# third-party-p2

[![pipeline status](https://gitlab.com/monochromata-de/third-party-p2/badges/master/pipeline.svg)](https://gitlab.com/monochromata-de/third-party-p2/commits/master)

A P2 repository that provides third-party libraries to Eclipse projects of monochromata

* An Eclipse p2 repository is available at https://monochromata-de.gitlab.io/third-party-p2/p2repo/ .