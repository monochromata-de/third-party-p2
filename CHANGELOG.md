# Release 1.0.215

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.9

# Release 1.0.214

* Update dependency org.slf4j:slf4j-api to v1.7.35

# Release 1.0.213

* Update dependency org.mockito:mockito-core to v4.3.1

# Release 1.0.212

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.8.3

# Release 1.0.211

* Update dependency org.mockito:mockito-core to v4.3.0

# Release 1.0.210

* Update dependency net.bytebuddy:byte-buddy to v1.12.7

# Release 1.0.209

* Update dependency org.slf4j:slf4j-api to v1.7.33

# Release 1.0.208

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.8.2

# Release 1.0.207

* Update dependency org.reficio:p2-maven-plugin to v2

# Release 1.0.206

* Update dependency org.assertj:assertj-core to v3.22.0

# Release 1.0.205

* Update dependency net.bytebuddy:byte-buddy to v1.12.6

# Release 1.0.204

* Update dependency net.bytebuddy:byte-buddy to v1.12.5

# Release 1.0.203

* Update dependency org.mockito:mockito-core to v4.2.0

# Release 1.0.202

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.8.1

# Release 1.0.201

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.8

# Release 1.0.200

* Update dependency net.bytebuddy:byte-buddy to v1.12.3

# Release 1.0.199

* Update dependency net.bytebuddy:byte-buddy to v1.12.2

# Release 1.0.198

* Update dependency org.mockito:mockito-core to v4.1.0

# Release 1.0.197

* Update dependency net.bytebuddy:byte-buddy to v1.12.1

# Release 1.0.196

* Update dependency net.bytebuddy:byte-buddy to v1.12.0

# Release 1.0.195

* Update dependency net.bytebuddy:byte-buddy to v1.11.22

# Release 1.0.194

* Update dependency net.bytebuddy:byte-buddy to v1.11.21

# Release 1.0.193

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.7.2

# Release 1.0.192

* Update dependency net.bytebuddy:byte-buddy to v1.11.20

# Release 1.0.191

* Update dependency org.mockito:mockito-core to v4

# Release 1.0.190

* Update dependency net.bytebuddy:byte-buddy to v1.11.19

# Release 1.0.189

* Update dependency net.bytebuddy:byte-buddy to v1.11.18

# Release 1.0.188

* Update dependency org.assertj:assertj-core to v3.21.0

# Release 1.0.187

* Update dependency net.bytebuddy:byte-buddy to v1.11.16

# Release 1.0.186

* Update dependency net.bytebuddy:byte-buddy to v1.11.15

# Release 1.0.185

* Update dependency net.bytebuddy:byte-buddy to v1.11.14

# Release 1.0.184

* Update dependency org.mockito:mockito-core to v3.12.4

# Release 1.0.183

* Update dependency org.mockito:mockito-core to v3.12.1

# Release 1.0.182

* Update dependency net.bytebuddy:byte-buddy to v1.11.13

# Release 1.0.181

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.7.1

# Release 1.0.180

* Update dependency net.bytebuddy:byte-buddy to v1.11.12

# Release 1.0.179

* Update dependency net.bytebuddy:byte-buddy to v1.11.9

# Release 1.0.178

* Update dependency org.slf4j:slf4j-api to v1.7.32

# Release 1.0.177

* Update dependency net.bytebuddy:byte-buddy to v1.11.8

# Release 1.0.176

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.7

# Release 1.0.175

* Update dependency net.bytebuddy:byte-buddy to v1.11.6

# Release 1.0.174

* Update dependency org.mockito:mockito-core to v3.11.2

# Release 1.0.173

* Update dependency org.assertj:assertj-core to v3.20.2

# Release 1.0.172

* Update dependency net.bytebuddy:byte-buddy to v1.11.5

# Release 1.0.171

* Update dependency org.objenesis:objenesis to v3.2

# Release 1.0.170

* Update dependency org.apache.commons:commons-lang3 to v3.12.0

# Release 1.0.169

* Update dependency org.slf4j:slf4j-api to v1.7.31

# Release 1.0.168

* Update dependency net.bytebuddy:byte-buddy to v1.11.3

# Release 1.0.167

* Update dependency org.assertj:assertj-core to v3.20.1

# Release 1.0.166

* Update dependency org.mockito:mockito-core to v3.11.1

# Release 1.0.165

* Update dependency net.bytebuddy:byte-buddy to v1.11.2

# Release 1.0.164

* Update dependency org.mockito:mockito-core to v3.11.0

# Release 1.0.163

* Update dependency net.bytebuddy:byte-buddy to v1.11.1

# Release 1.0.162

* Update dependency org.reficio:p2-maven-plugin to v1.7.0

# Release 1.0.161

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.6.1

# Release 1.0.160

* Update dependency org.mockito:mockito-core to v3.10.0

# Release 1.0.159

* Update dependency org.reficio:p2-maven-plugin to v1.6.0

# Release 1.0.158

* Update dependency net.bytebuddy:byte-buddy to v1.11.0

# Release 1.0.157

* Update dependency org.mockito:mockito-core to v3.9.0

# Release 1.0.156

* Update dependency net.bytebuddy:byte-buddy to v1.10.22

# Release 1.0.155

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.5

# Release 1.0.154

* Update dependency org.mockito:mockito-core to v3.8.0

# Release 1.0.153

* Update dependency net.bytebuddy:byte-buddy to v1.10.21

# Release 1.0.152

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.4

# Release 1.0.151

* Update dependency net.bytebuddy:byte-buddy to v1.10.20

# Release 1.0.150

* Update dependency org.assertj:assertj-core to v3.19.0

# Release 1.0.149

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.2

# Release 1.0.148

* Update dependency org.mockito:mockito-core to v3.7.7

# Release 1.0.147

* Update dependency org.mockito:mockito-core to v3.7.0

# Release 1.0.146

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.1

# Release 1.0.145

* Update dependency net.bytebuddy:byte-buddy to v1.10.19

# Release 1.0.144

* Correct p2 path
* Correct p2 path
* Update dependencies and publish to GitLab Pages

# Release 1.0.143

* Update dependency org.mockito:mockito-core to v3.6.28

# Release 1.0.142

* Update dependency org.assertj:assertj-core to v3.18.1

# Release 1.0.141

* Update dependency net.bytebuddy:byte-buddy to v1.10.18

# Release 1.0.140

* Update dependency org.mockito:mockito-core to v3.6.0

# Release 1.0.139

* Update dependency org.assertj:assertj-core to v3.18.0

# Release 1.0.138

* Update dependency org.mockito:mockito-core to v3.5.15

# Release 1.0.137

* Update dependency net.bytebuddy:byte-buddy to v1.10.17

# Release 1.0.136

* Update dependency org.mockito:mockito-core to v3.5.13

# Release 1.0.135

* Update dependency net.bytebuddy:byte-buddy to v1.10.16

# Release 1.0.134

* Update dependency net.bytebuddy:byte-buddy to v1.10.15

# Release 1.0.133

* Update dependency org.mockito:mockito-core to v3.5.11

# Release 1.0.132

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.4.3

# Release 1.0.131

* Update dependency org.assertj:assertj-core to v3.17.2

# Release 1.0.130

* Update dependency org.mockito:mockito-core to v3.5.10

# Release 1.0.129

* Update dependency org.mockito:mockito-core to v3.5.9

# Release 1.0.128

* Update dependency org.assertj:assertj-core to v3.17.1

# Release 1.0.127

* Update dependency org.mockito:mockito-core to v3.5.7

# Release 1.0.126

* Update dependency org.assertj:assertj-core to v3.17.0

# Release 1.0.125

* Update dependency org.mockito:mockito-core to v3.5.5

# Release 1.0.124

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.4.2

# Release 1.0.123

* Update dependency org.mockito:mockito-core to v3.5.2

# Release 1.0.122

* Update dependency org.mockito:mockito-core to v3.5.0

# Release 1.0.121

* Update dependency net.bytebuddy:byte-buddy to v1.10.14

# Release 1.0.120

* Update dependency org.mockito:mockito-core to v3.4.6

# Release 1.0.119

* Update dependency org.mockito:mockito-core to v3.4.4

# Release 1.0.118

* Update dependency org.mockito:mockito-core to v3.4.3

# Release 1.0.117

* Update dependency org.mockito:mockito-core to v3.4.0

# Release 1.0.116

* Update dependency net.bytebuddy:byte-buddy to v1.10.13

# Release 1.0.115

* Update dependency net.bytebuddy:byte-buddy to v1.10.12

# Release 1.0.114

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.4.1

# Release 1.0.113

* Update dependency net.bytebuddy:byte-buddy to v1.10.11

# Release 1.0.112

* Update dependency org.assertj:assertj-core to v3.16.1

# Release 1.0.111

* Update dependency org.assertj:assertj-core to v3.16.0

# Release 1.0.110

* Update dependency net.bytebuddy:byte-buddy to v1.10.10

# Release 1.0.109

* Update dependency org.apache.maven.wagon:wagon-ssh to v3.4.0

# Release 1.0.108

* Update dependency net.bytebuddy:byte-buddy to v1.10.9

# Release 1.0.107

* Remove dependenciesManagement section
* Remove java-contract - it has its own p2repo now

# Release 1.0.106

* Manually update java-contract to 1.0.2

# Release 1.0.105

* Update dependency org.apache.commons:commons-lang3 to v3.10

# Release 1.0.104

* Manually update java-contract to 0.5.30

# Release 1.0.103

* Manually update java-contract to 0.5.29

# Release 1.0.102

* Update dependency de.monochromata.contract:java-contract to v0.5.28

# Release 1.0.101

* Update dependency de.monochromata.contract:java-contract to v0.5.28

# Release 1.0.100

* Update dependency de.monochromata.contract:java-contract to v0.5.27

# Release 1.0.99

* Update dependency de.monochromata.contract:java-contract to v0.5.27

# Release 1.0.98

* Update dependency de.monochromata.contract:java-contract to v0.5.25

# Release 1.0.97

* Update dependency de.monochromata.contract:java-contract to v0.5.25

# Release 1.0.96

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.1.13

# Release 1.0.95

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.1.13

# Release 1.0.94

* Update dependency org.mockito:mockito-core to v3.3.3

# Release 1.0.93

* Update dependency org.mockito:mockito-core to v3.3.3

# Release 1.0.92

* Update dependency de.monochromata.contract:java-contract to v0.5.20

# Release 1.0.91

* Update dependency de.monochromata.contract:java-contract to v0.5.20

# Release 1.0.90

* Update dependency org.mockito:mockito-core to v3.3.0

# Release 1.0.89

* Update dependency org.assertj:assertj-core to v3.15.0

# Release 1.0.88

* Update dependency net.bytebuddy:byte-buddy to v1.10.8

# Release 1.0.87

* Update dependency de.monochromata.contract:java-contract to v0.5.18

# Release 1.0.86

* Update dependency de.monochromata.contract:java-contract to v0.5.18

# Release 1.0.85

* Add regexManager for p2repo to renovate.json

# Release 1.0.84

* Update to java-contract 0.5.17

# Release 1.0.83

* Update dependency de.monochromata.contract:java-contract to v0.5.16

# Release 1.0.82

* Update java-contract 0.5.12 in p2 repo

# Release 1.0.81

* Update dependency de.monochromata.contract:java-contract to v0.5.12

# Release 1.0.80

* Update dependency de.monochromata.contract:java-contract to v0.5.11

# Release 1.0.79

* Update dependency de.monochromata.contract:java-contract to v0.5.10

# Release 1.0.78

* Update dependency org.mockito:mockito-core to v3.3.0

# Release 1.0.77

* Update dependency de.monochromata.contract:java-contract to v0.5.9

# Release 1.0.76

* Update dependency net.bytebuddy:byte-buddy to v1.10.8

# Release 1.0.75

* Update dependency de.monochromata.contract:java-contract to v0.5.7

# Release 1.0.74

* Update to java-contract 0.5.5

# Release 1.0.73

* Upgrade to java-contract 0.5.4

# Release 1.0.72

* Upgrade to java-contract 0.5.3

# Release 1.0.71

* Update java-contract to 0.5.2

# Release 1.0.70

* Update java-contract to 0.5.0 and p2repo to latest versions

# Release 1.0.69

* Update dependency de.monochromata.contract:java-contract to v0.4.31

# Release 1.0.68

* Update dependency de.monochromata.contract:java-contract to v0.4.29

# Release 1.0.67

* Update dependency de.monochromata.contract:java-contract to v0.4.28

# Release 1.0.66

* Update dependency de.monochromata.contract:java-contract to v0.4.26

# Release 1.0.65

* Update dependency de.monochromata.contract:java-contract to v0.4.25

# Release 1.0.64

* Update dependency org.assertj:assertj-core to v3.15.0

# Release 1.0.63

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.1.12

# Release 1.0.62

* No changes

# Release 1.0.61

* No changes

# Release 1.0.60

* No changes

# Release 1.0.59

* No changes

# Release 1.0.58

* No changes

# Release 1.0.57

* No changes

# Release 1.0.56

* No changes

# Release 1.0.55

* No changes

# Release 1.0.54

* No changes

# Release 1.0.53

* No changes

# Release 1.0.52

* No changes

# Release 1.0.51

* No changes

# Release 1.0.50

* No changes

# Release 1.0.49

* No changes

# Release 1.0.48

* No changes

# Release 1.0.47

* No changes

# Release 1.0.46

* No changes

# Release 1.0.45

* No changes

# Release 1.0.44

* No changes

# Release 1.0.43

* No changes

# Release 1.0.42

* No changes

# Release 1.0.41

* No changes

# Release 1.0.40

* No changes

# Release 1.0.39

* No changes

# Release 1.0.38

* No changes

# Release 1.0.37

* No changes

# Release 1.0.36

* No changes

# Release 1.0.35

* No changes

# Release 1.0.34

* No changes

# Release 1.0.33

* No changes

# Release 1.0.32

* No changes

# Release 1.0.31

* No changes

# Release 1.0.30

* No changes

# Release 1.0.29

* No changes

# Release 1.0.28

* No changes

# Release 1.0.27

* No changes

# Release 1.0.26

* No changes

# Release 1.0.25

* No changes

# Release 1.0.24

* No changes

# Release 1.0.23

* No changes

# Release 1.0.22

* No changes

# Release 1.0.21

* No changes

# Release 1.0.20

* No changes

# Release 1.0.19

* No changes

# Release 1.0.18

* No changes

# Release 1.0.17

* No changes

